describe('rootsQA', function(){
    var testData = [
        [2, 4, 2, -1],
        [3.2, -7.8, 1, [2.3, 0.14]],
        [8, 4, 2, 'no roots']
    ];
    testData.forEach(function (elm, i) {
        it('input data: ' + 'a = ' + testData[i][0] + ', b = ' + testData[i][1] + ', c= ' + testData[i][2] + '; should return value = ' + testData[i][3] + ' and returned value = ' + rootsQA(elm[0], elm[1], elm[2]),
            function() {
            assert.equal(rootsQA(elm[0], elm[1], elm[2]).toString(), elm[3].toString());
        });
    });

});

describe('removeDoubledChars', function(){
    var testData = [
        ['ttest strring', 'test string'],
        ['test sttring', 'test string'],
        ['tessst stringgg', 'tesst stringg'],
    ];
    testData.forEach(function (elm, i) {
        it('input data: ' +  testData[i][0] + ' should return value = ' + testData[i][1] + ' and returned value = ' + removeDoubledChars(elm[0]),
            function() {
            assert.equal(removeDoubledChars(elm[0]), elm[1]);
        });
    });

});

describe('removeNotUniqueChars', function(){
    var testData = [
        ['test string', 'e ring'],
        ['test1 s t2ring', 'e12ring'],
        ['tes1t str2ing123', 'e ring3'],
    ];
    testData.forEach(function (elm, i) {
        it('input data: ' +  testData[i][0] + ' should return value = ' + testData[i][1] + ' and returned value = ' + removeNotUniqueChars(elm[0]),
            function() {
            assert.equal(removeNotUniqueChars(elm[0]), elm[1]);
        });
    });

});

describe('reverseString', function(){
    var testData = [
        ['test string', 'gnirts tset'],
        ['123456', '654321'],
        ['tt44vst', 'tsv44tt'],
    ];
    testData.forEach(function (elm, i) {
        it('input data: ' +  testData[i][0] + ' should return value = ' + testData[i][1] + ' and returned value = ' + reverseString(elm[0]),
            function() {
            assert.equal(reverseString(elm[0]), elm[1]);
        });
    });

});

describe('insertString', function(){
    var testData = [
        ['test string', 1,    '00',  '00test string' ],
        ['123456',      2,    'Aa',  '1Aa23456'],
        ['tt44vst',     7,    'Bbb', 'tt44vsBbbt'],
        ['tt44vst',     'r',  'Q',   'not valid input data'],
        ['tt44vst',     '-1', 'Q',   'not valid input data']
    ];
    testData.forEach(function (elm, i) {
        it('input data: ' +  testData[i][0] +', '+ testData[i][1] +', '+  testData[i][2] + ' should return value = ' + testData[i][3] + ' and returned value = ' + insertString(elm[0],elm[1],elm[2]),
            function() {
            assert.equal(insertString(elm[0],elm[1],elm[2]), elm[3]);
        });
    });
});

describe('arrMinMax', function(){
    var testData = [
        [[1, 2, 3], [1, 3]],
        [[1, 5, 3], [1, 5]],
        [[5, 2, 3], [2, 5]]
    ];
    testData.forEach(function (elm, i) {
        it('input data: ' +  testData[i][0] + ' should return value = ' + testData[i][1] + ' and returned value = ' + arrMinMax(elm[0]),
            function() {
            assert.equal(arrMinMax(elm[0]).toString(), elm[1].toString());
        });
    });
});

describe('makeNewArr', function(){
    var testData = [
        [['ttt', '1234', '12345'], ['1234', '12345']],
        [['ttt', '1234', '12345', 'fghjkdd'], ['1234', '12345', 'fghjkdd']],
        [['ttt', '1234', '12'], ['1234']],
    ];
    testData.forEach(function (elm, i) {
        it('input data: ' +  testData[i][0] + ' should return value = ' + testData[i][1] + ' and returned value = ' + makeNewArr(elm[0]),
            function() {
            assert.equal(makeNewArr(elm[0]).toString(), elm[1].toString());
        });
    });
});

describe('randomArray', function(){
    var testData = [
        [[10, 15, 75, 3, 5], ['1234', '12345']],
        [['ttt', '1234', '12345', 'fghjkdd'], ['1234', '12345', 'fghjkdd']],
        [['ttt', '1234', '12'], ['1234']],
    ];
    testData.forEach(function (elm, i) {
        it('input data: ' +  testData[i][0] + ' should return value = ' + testData[i][1] + ' and returned value = ' + randomArray(elm[0]),
            function() {
            assert.equal(randomArray(elm[0]).toString(), elm[1].toString());
        });
    });
});

