"use strict";

// ЧИСЛА
// Найти корни квадратного уравнения a*x^2 + b*x + c. Значения a, b и c выбираете сами.

var rootsQA = function(a,b,c){
    var x0, x1;
    var d = b*b - 4*a*c;
    if (d > 0){
        x0 = +((-b + Math.sqrt(d)) / (2*a)).toFixed(2);
        x1 = +((-b - Math.sqrt(d)) / (2*a)).toFixed(2);
        return [x0, x1];
    } else if(d == 0){
        x0 = +(-b / (2*a)).toFixed(2);
        return x0;
    } else {
        return 'no roots';
    }
};

// СТРОКИ
// Дана строка, удалить из нее все повторяющиеся символы
// вариант для задвоившихся символов (не учитываем затроившиеся и так далее повторы, больше 2 раз)
var removeDoubledChars = function(str){
    var arr = str.split('');

    for (var i=0; i<arr.length - 2; i++){
        if (arr[i] === arr[i+1]) {
            arr.splice(i, 1);
        }
    }
    return arr.join('');
};

// вариант 2 - удалить все символы, встречающиеся в строке более одного раза
var removeNotUniqueChars = function(str){
    var arr = str.split('');
    var isUnique;
    var newArr = [];

    for (var i=0; i<=arr.length - 1; i++){
        isUnique = true;

        for (var j=0; j<=arr.length - 1; j++)
            if (arr[i] === arr[j] && i!=j) {
                isUnique = false;
            }

        if(isUnique){
            newArr.push(arr[i]);
        }
    }
    return newArr.join('');
};

//Перевернуть строку, т.е. последние символы должны стать первыми, а первые последними.
var reverseString = function(str){
    return str.split('').reverse().join('');
};

//Вставить в заданную позицию строки другую строку
var insertString = function(str, position, strInsert){
    if (position <= 0 || position>str.length || (typeof position != "number")) return 'not valid input data';
    var strBegin = str.slice(0, position-1);
    var strEnd = str.slice(position-1, str.length);
    return strBegin + strInsert + strEnd;
};

// МАССИВЫ
// Дан массив целых чисел. Найти минимальное и максимальное значения
var arrMinMax = function (arr) {
    var min = arr[0], max = arr[0];
      for (var i = 1; i < arr.length; i++){
            if (arr[i] > max){
                max = arr[i];
            } else if (arr[i] < min) {
                min = arr[i];
            }
      }

     return [min, max];
};

// Дан массив строк. Нужно создать новый массив из строк первого массива, длина которых больше 3 символов.
var makeNewArr = function(arr){
    var newArr = [];
    for (var i=0; i < arr.length; i++){
        if (arr[i].length > 3){
            newArr.push(arr[i]);
        }
    }
    return newArr;
};

// Создать одномерный массив и наполнить его случайными числами от 15 до 75, но если случайное число кратно 3 -
// то заменяем его на 3, а если кратно 5 - то на 5.
// Решить эту задачу тремя вариантами через разные циклы (for, while, do/while)

var randomArray = function(length, min, max, num1, num2){
    var arr = [], el;
    for (var i=0; i < length; i++){
        el = Math.round(Math.random()*(max - min) + min);

        if (el % num1 === 0){
            arr.push(num1)
        } else if (el % num2 === 0) {
            arr.push(num2)
        } else {
            arr.push(el);
        }
    }
    return arr;
}

console.log(randomArray(10, 15, 75, 3, 5));

// WHILE
var randomArrayWhile = function(length, min, max, num1, num2){
    var arr = [], el, i = 0;
    while (i < length){
        el = Math.round(Math.random()*(max - min) + min);
        if (el % num1 === 0){
            arr.push(num1)
        } else if (el % num2 === 0) {
            arr.push(num2)
        } else {
            arr.push(el);
        }
        i++
    }
    return arr;
}

console.log(randomArrayWhile(10, 15, 75, 3, 5));

// DO WHILE
var randomArrayWhile = function(length, min, max, num1, num2){
    var arr = [], el, i = 0;
    do {
        el = Math.round(Math.random()*(max - min) + min);
        if (el % num1 === 0){
            arr.push(num1)
        } else if (el % num2 === 0) {
            arr.push(num2)
        } else {
            arr.push(el);
        }
        i++
    } while (i < length)
    return arr;
}

console.log(randomArrayWhile(10, 15, 75, 3, 5));

// ОБЪЕКТЫ
// Создать массив объектов employees. Каждый объект должен иметь свойства: name, position, salary,
// startWorkDate, vacationDays и наполнить его значениями. Необходимо найти сотрудников,
// у которых vacationDays > 7, добавить им свойство needHaveRest: true и вывести их имена.

var makeEmployeesArr = function(employeesData){
    var employees = [];

    for (var i=0; i < employeesData.length; i++){
        var employee = {};
        employee.name = employeesData[i][0];
        employee.position = employeesData[i][1];
        employee.salary = employeesData[i][2];
        employee.startWorkDate = employeesData[i][3];
        employee.vacationDays = employeesData[i][4];

        employees.push(employee);
    }

    for (var i=0; i< employees.length; i++){
        if (employees[i].vacationDays > 7){
            employees[i].needHaveRest = true;
            console.log(employees[i].name);
        }
    }
};

makeEmployeesArr([['Tom','manager','15K', 12, 8],['Maria','manager','16K', 10, 6],['John','recruiter','14K', 3, 10]]);