"use strict";

x = parseInt(x); // преобразование строки в целое число
x = +x; // преобразование любого типа в число
console.log(isNaN(x)); // проверка на NaN; NaN - Not-a-Number

var first = '0.1';
first = parseFloat(first); // преобразование строки в вещественное число

var second = 0.2564;
second = second.toFixed(3); // округление до 3-го знака после запятой
var t = Math.round(0.4); // округление до целого числа
// Math.ceil(); округление в большую сторону
// Math.floor(); округление в меньшую сторону
// ( 1 / 0 ) = Infinity
// ( -1 / 0 ) = -Infinity
// проверить isFinite(number) - вернет true либо false

/** Строки */
var str = 'qsajkd#gsajk';
console.log(str.length); // длина строки
console.log(str.charAt(3)); // доступ к символу по порядковому индексу
console.log(str.toUpperCase()); // перевести строку в верхний регистр
console.log(str.toLowerCase()); // перевести строку в нижний регистр

// найти вхождение подстроки в строку - вернет порядковый индекс либо -1
// вторыс аргументом метода является позиция с которой нужно начать поиск, например: str.indexOf('#', 2)
if (str.indexOf('#') === -1) {
    console.log('EMPTY');
} else {
    console.log('OK');
}

// пример мспользования хешей
// var password = 'qwerty';
// var hash = sha256(password);
//
// if (hash === sha256(password)) {
//
// }


var hash = '34856843eiie#0948r09325#21654#3214832';
// разбиваем строку на части по символу #
// пример 1
if (hash.indexOf('#') !== -1) {
    hash = hash.slice(0, hash.indexOf('#')); // вырезаем из строки hash подстроку с 0го индекса по #
}
// пример 2
hash = hash.split('#');
console.log(hash);
console.log(hash.join('')); // массив строк соединяем в одну строку

/** Объекты */
var obj = {
    name: "Viktor",
    age: [],
    qwerty: {list: []},
    condition: {
        first: 'dsfsd'
    }
}; // инициализация обекта

// добавляем новые свойствав
obj.city = 'Dnepr';
obj.country = 'Ukraine';

var key2 = 'name';
console.log(obj['name'], obj.name, obj[key2]); // 3 варианта дотсупа к свойствам объектов

delete obj.name; // удаление свойства объекта

// обход свойств
for (var key in obj) {
    if (key.indexOf('a') !== -1) {
        // console.log(key, obj[key]);
    }
}
// проверка на существование свойства
// вариант 1
if ('name' in obj) {

}
// вариант 2
if (obj[key2] !== undefined) {

}

if(obj.hasOwnProperty()){

}

console.log(Object.keys(obj)); // получить массив имен свойств объекта

// копирование объекта (объекты передаются по ссылке)
var objCopy = {};
for (var key in obj) {
    objCopy[key] = obj[key];
}

objCopy.city = 'Kyiv';
// console.log(obj, objCopy);


/** Массивы */
// Инициализация массивов 3 варианта
var mass = [2,5,7,3];
var mass1 = new Array(2); // [undefined,undefined]
var mass2 = new Array('q','w', 'r'); // ['q','w','r'];
// console.log(mass, mass1[0], mass1[1], mass2);

// push - доьбавить в конец, pop - забрать с конца, shift - забрать с начала, unshift - добавить в начало
mass.push('qwerty');
var elm = mass.pop();

// очередь
var mass3 = [];
// I - цикл for (начальное значение; условие завершения; шаг)
for (var i = 0; i < 10; i++) {
    mass3.push(i + 2);
}
// II цикл while (условие захода в цикл) - может не выполниться ни разу
i = 0;
while (i < 2) {
    // todo something
    i++;
}
// II цикл do/while (условие продолжения цикла) - в любом случае выполнится один раз
i = 0;
do {
    // todo something
    i++;
} while(i < 2);

// копирование массива
var massCopy = [].concat(mass3);
// обход массива
massCopy.forEach(function (elm, index) {
    console.log(index, elm);
});
// обходим массив и создаем новый массив из результатов выполнения коллбека (return elm + '*' + index)
var newMass = massCopy.map(function (elm, index) {
    return elm + '*' + index;
});
// обходим массив и создаем новый массив из элементов, для которых коллбек вернет true (все элементы которые > 7)
var newMass1 = massCopy.filter(function (elm, index) {
    return elm > 7;
});

console.log(newMass);