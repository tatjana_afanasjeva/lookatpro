describe('Pow', function() {
    it('should be an error', function() {
        (function() {
            pow(9, 'sd');
        }).should.throw();
    });

    var testData = [
        [0, 5, 0],
        [1, 2, 1],
        [2, 2, 4],
        [3, 2, 10]
    ];
    testData.forEach(function (elm) {
        it('should return true value', function() {
            assert.equal(pow(elm[0], elm[1]), elm[2]);
        });
    });
});