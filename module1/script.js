"use strict;"

mocha.setup("bdd");
var assert = chai.assert;
var expect = chai.expect;
chai.should();

var pow = function (num, st) {
    if (isNaN(+st)) {
        throw new Error('error');
    }
    return Math.pow(num, st);
};
