"use strict;"

mocha.setup("bdd");
var assert = chai.assert;
var expect = chai.expect;
chai.should();

var pow = function (num, st) {
    if (isNaN(+st)) {
        throw new Error('error');
    }
    return Math.pow(num, st);
};

function factorial(num){
    if(num==1){
        return num;
    } else {
        return num * factorial(num - 1);
    }
};

function powRecursion(num, p) {
    return num * ((p === 1) ? 1 : powRecursion(num, p - 1));
}
