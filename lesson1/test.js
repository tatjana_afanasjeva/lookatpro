/*describe('Pow', function() {
    it('should be an error', function() {
        (function() {
            pow(9, 'sd');
        }).should.throw();
    });

    var testData = [
        [0, 5, 0],
        [1, 2, 1],
        [2, 2, 4],
        [3, 2, 1]
    ];
    testData.forEach(function (elm) {
        it('should return true value', function() {
            assert.equal(pow(elm[0], elm[1]), elm[2]);
        });
    });
});*/

describe('Factorial', function(){
    var testData = [
        [1, 1],
        [2, 2],
        [3, 6],
        [4, 24]
    ];
    testData.forEach(function (elm, i) {
        it('should return true value ' + testData[i][0] + '! = ' + testData[i][1], function() {
            assert.equal(factorial(elm[0]), elm[1]);
        });
    });

});

describe('powRecursion', function(){
    var testData = [
        [1, 1],
        [2, 2],
        [3, 2],
        [2, 3]
    ];
    testData.forEach(function (elm, i) {
        it('should return true value ' + testData[i][0] + '^' + testData[i][1] +' = ' + Math.pow(elm[0],elm[1]) , function() {
            assert.equal(powRecursion(elm[0], elm[1]), Math.pow(elm[0],elm[1]));
        });
    });

});

